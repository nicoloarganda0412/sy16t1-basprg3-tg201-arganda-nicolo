#include "Planet.h"


Planet::Planet(SceneNode* node)
{
	mNode = node;
	mParent = NULL;
}

// Create a procedural object with material and lighting

Planet * Planet::createPlanet(SceneManager * sceneMgr, const std::string & name, float size, ColourValue colourMat)
{
	ManualObject * planet = sceneMgr->createManualObject();

	//Material 

		MaterialPtr manualObjMaterial = Ogre::MaterialManager::getSingleton().create(name, "General");
		manualObjMaterial->setReceiveShadows(true);
		manualObjMaterial->getTechnique(0)->setLightingEnabled(true);
		manualObjMaterial->getTechnique(0)->getPass(0)->setDiffuse(colourMat);

		if (name == "sun")
		{
			planet->begin(name, RenderOperation::OT_TRIANGLE_LIST);
			manualObjMaterial->getTechnique(0)->getPass(0)->setEmissive(colourMat);

			// Referenced from http://www.ogre3d.org/forums/viewtopic.php?f=2&t=76621
		}

		else
		{
			planet->begin(name, RenderOperation::OT_TRIANGLE_LIST);
		}

		// Attaches Node to the Obj
		SceneNode* node = sceneMgr->getRootSceneNode()->createChildSceneNode();;
		node->attachObject(planet);

		// C R E A T E S  A  S P H E R E

		// Latitude
		int nRings = 30;
		// Longitude
		int nSegments = 30;

		float fDeltaRingAngle = (Math::PI / nRings);
		float fDeltaSegAngle = (2 * Math::PI / nSegments);
		unsigned short wVerticeIndex = 0;
	
	// Generate the group of rings for the sphere
	for (int ring = 0; ring <= nRings; ring++) 
	{
		
		float r0 = size * Math::Sin(ring * fDeltaRingAngle);
		// Y-Axis
		float y0 = size * Math::Cos(ring * fDeltaRingAngle);

		// Generate the group of segments for the current ring
		for (int seg = 0; seg <= nSegments; seg++)
		{
			// X-Axis
			float x0 = r0 * Math::Sin(seg * fDeltaSegAngle);
			// Z-Axis
			float z0 = r0 * Math::Cos(seg * fDeltaSegAngle);
			
			planet->position(x0, y0, z0); // Sets vertices
			
			planet->normal(Vector3(x0, y0, z0).normalisedCopy()); // Normalises vertices using the built in function.

			//Index Buffers
			if (ring != nRings) 
			{
				planet->index(wVerticeIndex + nSegments + 1);
				planet->index(wVerticeIndex); 
				planet->index(wVerticeIndex + nSegments);
				planet->index(wVerticeIndex + nSegments + 1);
				planet->index(wVerticeIndex + 1);
				planet->index(wVerticeIndex);

				wVerticeIndex++;
			}

		//referenced from http://www.ogre3d.org/tikiwiki/ManualSphereMeshes

		}; // end for seg
	} // end for ring
	planet->end();

	return new Planet(node);
}

SceneNode * Planet::getNode()
{
	return mNode;
}

Planet * Planet::getParent()
{
	return mParent;
}

void Planet::update(const FrameEvent & evt)
{
	Degree rotation = Degree(mRotationSpeed);
	mNode->rotate(Vector3(0, 1, 0), Radian(rotation));



	if (mRevolutionSpeed != NULL || mParent != NULL)
	{
	
		Degree revolution = Degree(mRevolutionSpeed * evt.timeSinceLastFrame);
		float xPrev = mNode->getPosition().x - mParent->getNode()->getPosition().x;
		float zPrev = mNode->getPosition().z - mParent->getNode()->getPosition().z;

		//// creates the next set of coordinates
		float xNext = (xPrev * Math::Cos((Radian(revolution))) + (zPrev * Math::Sin(Radian(revolution))));
		float zNext = (xPrev * -Math::Sin((Radian(revolution))) + (zPrev * Math::Cos(Radian(revolution))));

		//// adds parent's location
		xNext += mParent->getNode()->getPosition().x;
		zNext += mParent->getNode()->getPosition().z;

	
		// translates 
		mNode->setPosition(xNext, 0, zNext);

	}
}

void Planet::setParent(Planet * parent)
{
	mParent = parent;
}

void Planet::setRevolutionSpeed(float days)
{
	float rev = 365 / days;
	rev *= 6;

	mRevolutionSpeed = rev;
}

void Planet::setRotationSpeed(float speed)
{
	mRotationSpeed = speed;
}
