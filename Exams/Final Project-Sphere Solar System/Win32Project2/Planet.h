#pragma once
#include "BaseApplication.h"
using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode* node);

	static Planet* createPlanet(SceneManager* sceneMgr, const std::string & name, float size, ColourValue colourMat);

	SceneNode *getNode();
	Planet* getParent();

	// "update" function translates the object to rotate relative to the parents position.
	void update(const FrameEvent & evt);
	void setParent(Planet* parent);
	void setRevolutionSpeed(float days);
	void setRotationSpeed(float speed);

private:
	SceneNode* mNode;
	Planet* mParent;
	float mRevolutionSpeed;
	float mRotationSpeed;
};

