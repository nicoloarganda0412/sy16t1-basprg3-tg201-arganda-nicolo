/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
___                 __    __ _ _    _
/___\__ _ _ __ ___  / / /\ \ (_) | _(_)
//  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
|___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "Planet.h"
#include <string>
#include <vector>
using namespace Ogre;
using namespace std;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	mCamera->setPolygonMode(PolygonMode::PM_SOLID);

	// Create your scene here :)
	
	// Creates light

	Light *pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 1.0f));
	// Attenuation intensifies the lighting
	pointLight->setAttenuation(3257, 1.0f, 0.0014f, 0.000007);
	pointLight->setCastShadows(true);

	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));

	Planet* Sun = Planet::createPlanet(mSceneMgr, "sun", 20, ColourValue(1, 1, 0));
	Sun->setRotationSpeed(0.2f);
	Sun->getNode();
	mSystem.push_back(Sun);
	// P L A N E T S
 
	// study the strip of vectors in the sphere

	// Input days to the revolution speed.
	Planet* Mercury = Planet::createPlanet(mSceneMgr, "mercury", 3,  ColourValue(0.82, 0.7, 0.54));
	Mercury->getNode();
	Mercury->setParent(Sun);
	Mercury->getNode()->translate(Vector3(50, 0, 0));
	Mercury->setRevolutionSpeed(88.0f);
	Mercury->setRotationSpeed(0.2f);
	mSystem.push_back(Mercury);

	Planet* Venus = Planet::createPlanet(mSceneMgr,"venus", 5, ColourValue(0.94, 0.9, 0.67));
	Venus->getNode();
	Venus->setParent(Sun);
	Venus->getNode()->translate(Vector3(90, 0, 0));
	Venus->setRevolutionSpeed(224.0f);
	Venus->setRotationSpeed(0.2f);
	mSystem.push_back(Venus);

	Planet* Earth = Planet::createPlanet(mSceneMgr,"earth", 10, ColourValue(0, 0, 1));
	Earth->getNode();
	Earth->setParent(Sun);
	Earth->getNode()->translate(Vector3(120, 0, 0));
	Earth->setRevolutionSpeed(365.0f);
	Earth->setRotationSpeed(0.2f);
	mSystem.push_back(Earth);

	Planet* Mars = Planet::createPlanet(mSceneMgr, "mars", 10, ColourValue(0.71, 0.25, 0.05));
	Mars->setParent(Sun);
	Mars->getNode()->translate(Vector3(160, 0, 0));
	Mars->setRevolutionSpeed(687.0f);
	Mars->setRotationSpeed(0.2f);
	mSystem.push_back(Mars);

	// Earth doesn't revolve around Earth
	Planet* Moon = Planet::createPlanet(mSceneMgr, "moon", 1, ColourValue(0.7,0.7,0.7));
	Moon->getNode();
	Moon->setParent(Earth);
	Moon->getNode()->translate(Vector3(140, 0, 0));
	Moon->setRevolutionSpeed(10.0f);
	Moon->setRotationSpeed(0.2f);
	mSystem.push_back(Moon);


}


bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	for (int i = 0; i < mSystem.size(); i++)
	{
		mSystem[i]->update(evt);
	}
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
