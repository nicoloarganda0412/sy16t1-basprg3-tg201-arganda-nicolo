/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	mCamera->setPolygonMode(PolygonMode::PM_SOLID);

	// Create your scene here :)

	ManualObject* manual = createCube(10);

	mObject = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mObject->attachObject(manual);
}
ManualObject * TutorialApplication::createCube(int length)
{
	ManualObject* manual = mSceneMgr->createManualObject("illuminati");

	// How the vertices will Render
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	// Define the vertices
	// manual->position(x, y, z);


	manual->position(-length, -length, 0.0);
	manual->position(length, -length, 0.0);
	manual->position(length, length, 0.0);
	manual->position(-length, length, 0.0);


	manual->position(-length, -length, length * 2);
	manual->position(length, -length, length * 2);
	manual->position(length, length, length * 2);
	manual->position(-length, length, length * 2);

	// I N D E X  B U F F E R
	//BACK
	manual->index(2);
	manual->index(1);
	manual->index(0);

	manual->index(2);
	manual->index(0);
	manual->index(3);

	//FRONT
	manual->index(4);
	manual->index(5);
	manual->index(6);

	manual->index(7);
	manual->index(4);
	manual->index(6);

	//RIGHT
	manual->index(6);
	manual->index(5);
	manual->index(1);

	manual->index(6);
	manual->index(1);
	manual->index(2);

	// LEFT
	manual->index(3);
	manual->index(4);
	manual->index(7);

	manual->index(0);
	manual->index(4);
	manual->index(3);

	//BOTTOM
	manual->index(3);
	manual->index(6);
	manual->index(2);

	manual->index(3);
	manual->index(7);
	manual->index(6);

	//TOP
	manual->index(0);
	manual->index(1);
	manual->index(5);

	manual->index(5);
	manual->index(4);
	manual->index(0);

	// Stops drawing
	manual->end();

	return manual;
}
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	

	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		mObject->translate(0, 0, -1 * evt.timeSinceLastFrame * 10);
	}

	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		mObject->translate(0, 0, 1 * evt.timeSinceLastFrame * 10);
	}

	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		mObject->translate(-1 * evt.timeSinceLastFrame * 10, 0, 0 );
	}

	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		mObject->translate(1 * evt.timeSinceLastFrame * 10, 0, 0);
	}

	Quaternion rotateX(Degree(sqrt(0.5)), Vector3::UNIT_X);

	if (mKeyboard->isKeyDown(OIS::KC_U))
	{
		mObject->rotate(rotateX);
	}
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
