/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <string>
using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	mCamera->setPolygonMode(PolygonMode::PM_SOLID);

	// Create your scene here :)

	ManualObject* manual = createCube(10);
	ManualObject* secmanual = createCube(30);

	
	mObject = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	//mObject->attachObject(manual);
	mObject->attachObject(secmanual);
	mSecObj = mObject->createChildSceneNode();
	mSecObj->attachObject(manual);
}
bool TutorialApplication::keyReleased(const OIS::KeyEvent & args)
{
	// Sets initial Speed
	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		mSpeed = 10;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		mSpeed = 10;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		mSpeed = 10;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		mSpeed = 10;
	}

	return true;
}
ManualObject * TutorialApplication::createCube(int length)
{
	ManualObject* manual = mSceneMgr->createManualObject();

	// How the vertices will Render
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	// Define the vertices
	// manual->position(x, y, z);

	manual->position(-(length / 2), -(length / 2), (length / 2));
	manual->colour(ColourValue::Green);
	manual->position((length / 2), -(length / 2), (length / 2));
	manual->colour(ColourValue::Green);
	manual->position((length / 2), (length / 2), (length / 2));
	manual->colour(ColourValue::Green);
	manual->position(-(length / 2), (length / 2), (length / 2));
	manual->colour(ColourValue::Green);

	manual->position(-(length / 2), -(length / 2), -(length / 2));
	manual->colour(ColourValue::Blue);
	manual->position((length / 2), -(length / 2), -(length / 2));
	manual->colour(ColourValue::Blue);
	manual->position((length / 2), (length / 2), -(length / 2));
	manual->colour(ColourValue::Blue);
	manual->position(-(length / 2), (length / 2), -(length / 2));
	manual->colour(ColourValue::Blue);



	// I N D E X  B U F F E R
	//BACK
	manual->index(0);
	manual->index(1);
	manual->index(2);

	manual->index(3);
	manual->index(0);
	manual->index(2);

	//FRONT
	manual->index(4);
	manual->index(0);
	manual->index(3);

	manual->index(7);
	manual->index(4);
	manual->index(3);

	//RIGHT
	manual->index(6);
	manual->index(5);
	manual->index(4);

	manual->index(6);
	manual->index(4);
	manual->index(7);

	// LEFT
	manual->index(6);
	manual->index(1);
	manual->index(5);

	manual->index(1);
	manual->index(6);
	manual->index(2);

	//BOTTOM
	manual->index(6);
	manual->index(7);
	manual->index(3);

	manual->index(2);
	manual->index(6);
	manual->index(3);

	//TOP
	manual->index(4);
	manual->index(1);
	manual->index(0);

	manual->index(4);
	manual->index(5);
	manual->index(1);

	// Stops drawing
	manual->end();

	return manual;
}
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	
	BaseApplication::frameStarted(evt);

	//M O V E M E N T
	Vector3 movement = Vector3::ZERO;
	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		//Multiply mSpeed to accelerate
		mSpeed += 10 * evt.timeSinceLastFrame;
		movement.z -= mSpeed;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_K))
	{

		mSpeed += 10 * evt.timeSinceLastFrame;
		movement.z += mSpeed;
	}

	if (mKeyboard->isKeyDown(OIS::KC_J))
	{

		mSpeed += 10 * evt.timeSinceLastFrame;
		movement.x += mSpeed;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_L))
	{

		mSpeed += 10 * evt.timeSinceLastFrame;
		movement.x -= mSpeed;
	}

	// moves mObject according to Vector3 Movement
	movement *= evt.timeSinceLastFrame;
	mObject->translate(movement);

	//R O T A T I O N 
	Degree rotation = Degree(30);
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4))
	{
		
			mObject->rotate(Vector3(0, 1, 0), Radian(rotation) * evt.timeSinceLastFrame);
	}

	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6))
	{
		mObject->rotate(Vector3(0, 1, 0), Radian(-rotation) * evt.timeSinceLastFrame);
	}

	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8))
	{
		mObject->rotate(Vector3(1, 0, 0), Radian(-rotation) * evt.timeSinceLastFrame);
	}

	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2))
	{
		mObject->rotate(Vector3(1, 0, 0), Radian(rotation) * evt.timeSinceLastFrame);
	}

	mSecObj->setPosition(30, 0, 0);
	
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD5))
	{
		mObject->rotate(Vector3(1, 0, 0), Radian(rotation)), Node::TransformSpace::TS_WORLD;
	}
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
