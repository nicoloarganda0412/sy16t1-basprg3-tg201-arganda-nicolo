/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <string>
using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	mCamera->setPolygonMode(PolygonMode::PM_WIREFRAME);

	// Setting the light
	Light *pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 0.0f));
	pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	pointLight->setAttenuation(325, 0.0f, 0.014f, 0.0007);
	pointLight->setCastShadows(false);

	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));


	// Creates a manual object
	ManualObject* manual = createCube(10);

	ManualObject* secondMan = createProceduralObject(10, 4);
	// Attaches Object
	mObject = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mObject->attachObject(manual);
	mObject->translate(20, 0, 0);

}
bool TutorialApplication::keyReleased(const OIS::KeyEvent & args)
{
	// Sets initial Speed
	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		mSpeed = 10;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		mSpeed = 10;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		mSpeed = 10;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		mSpeed = 10;
	}

	return true;
}
ManualObject * TutorialApplication::createCube(int length)
{
	ManualObject* manual = mSceneMgr->createManualObject();

	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create("manualMaterial", "General");
	// Modify some properties of materials
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(0, 1, 0, 0);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setSpecular(0.3, 0.3, 0.3, 0);

	// How the vertices will Render
	manual->begin("myManualObjectMaterial", RenderOperation::OT_TRIANGLE_LIST);

	// Define the vertices
	// manual->position(x, y, z);

	// Front
	manual->position(-(length / 2), -(length / 2), length / 2);
	manual->colour(ColourValue::White);
	manual->normal(Vector3(0, 0, 1));
	manual->position((length / 2), -(length / 2), length / 2);
	manual->normal(Vector3(0, 0, 1));
	manual->position((length / 2), (length / 2), length / 2);
	manual->normal(Vector3(0, 0, 1));
	manual->position(-(length / 2), (length / 2), length / 2);
	manual->normal(Vector3(0, 0, 1));
	// Right
	manual->position((length / 2), -(length / 2), -length / 2);
	manual->normal(Vector3(1, 0, 0));
	manual->position((length / 2), (length / 2), -length / 2);
	manual->normal(Vector3(1, 0, 0));
	manual->position((length / 2), (length / 2), length / 2);
	manual->normal(Vector3(1, 0, 0));
	manual->position((length / 2), -(length / 2), length / 2);
	manual->normal(Vector3(1, 0, 0));
	// Left
	manual->position(-(length / 2), -(length / 2), length / 2);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-(length / 2), (length / 2), length / 2);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-(length / 2), (length / 2), -length / 2);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-(length / 2), -(length / 2), -length / 2);
	manual->normal(Vector3(-1, 0, 0));
	// Top
	manual->position(-(length / 2), (length / 2), length / 2);
	manual->normal(Vector3(0, 1, 0));
	manual->position((length / 2), (length / 2), length / 2);
	manual->normal(Vector3(0, 1, 0));
	manual->position((length / 2), (length / 2), -length / 2);
	manual->normal(Vector3(0, 1, 0));
	manual->position(-(length / 2), (length / 2), -length / 2);
	manual->normal(Vector3(0, 1, 0));
	// Bottom
	manual->position(-(length / 2), -(length / 2), length / 2);
	manual->normal(Vector3(0, -1, 0));
	manual->position(-(length / 2), -(length / 2), -length / 2);
	manual->normal(Vector3(0, -1, 0));
	manual->position((length / 2), -(length / 2), -length / 2);
	manual->normal(Vector3(0, -1, 0));
	manual->position((length / 2), -(length / 2), length / 2);
	manual->normal(Vector3(0, -1, 0));
	// Back
	manual->position((length / 2), (length / 2), -length / 2);
	manual->normal(Vector3(0, 0, -1));
	manual->position((length / 2), -(length / 2), -length / 2);
	manual->normal(Vector3(0, 0, -1));
	manual->position(-(length / 2), -(length / 2), -length / 2);
	manual->normal(Vector3(0, 0, -1));
	manual->position(-(length / 2), (length / 2), -length / 2);
	manual->normal(Vector3(0, 0, -1));
	// define usage of vertices by refering to the indexes
	// Front
	manual->index(0);
	manual->index(1);
	manual->index(2);
	manual->index(2);
	manual->index(3);
	manual->index(0);
	// Right
	manual->index(4);
	manual->index(5);
	manual->index(6);
	manual->index(6);
	manual->index(7);
	manual->index(4);
	// Left
	manual->index(8);
	manual->index(9);
	manual->index(10);
	manual->index(10);
	manual->index(11);
	manual->index(8);
	// Top
	manual->index(12);
	manual->index(13);
	manual->index(14);
	manual->index(14);
	manual->index(15);
	manual->index(12);
	// Bottom
	manual->index(16);
	manual->index(17);
	manual->index(18);
	manual->index(18);
	manual->index(19);
	manual->index(16);
	// Back
	manual->index(20);
	manual->index(21);
	manual->index(22);
	manual->index(22);
	manual->index(23);
	manual->index(20);
	// Stops drawing
	manual->end();


	return manual;
}

ManualObject * TutorialApplication::createProceduralObject(float length, int sides)
{
	ManualObject* manual = mSceneMgr->createManualObject();

	manual->begin("myManualObjectMaterial", RenderOperation::OT_POINT_LIST);

	//float xNext = (xPrevious * Math::Cos((Radian(rev))) + zPrevious * Math::Sin(Radian(rev)));
	//float zNext = (xPrevious * -Math::Sin((Radian(rev))) + zPrevious * Math::Cos(Radian(rev)));

	

	for (int i; i = 1; i <= sides)
	{
		// Sets Vertices
		float dummyX = length / 2 * Math::Cos((Radian(30))) + (-length / 2) * Math::Sin(Radian(30));
		float dummyY = length / 2 * -Math::Sin((Radian(30))) + (-length / 2) * Math::Sin(Radian(30));
		manual->position(dummyX, dummyY, length / 2);


	}

	manual->end();
	return manual;
}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	
	BaseApplication::frameStarted(evt);

	//M O V E M E N T
	Vector3 movement = Vector3::ZERO;
	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		//Multiply mSpeed to accelerate
		mSpeed += 10 * evt.timeSinceLastFrame;
		movement.z -= mSpeed;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_K))
	{

		mSpeed += 10 * evt.timeSinceLastFrame;
		movement.z += mSpeed;
	}

	if (mKeyboard->isKeyDown(OIS::KC_J))
	{

		mSpeed += 10 * evt.timeSinceLastFrame;
		movement.x += mSpeed;
	}

	else if (mKeyboard->isKeyDown(OIS::KC_L))
	{

		mSpeed += 10 * evt.timeSinceLastFrame;
		movement.x -= mSpeed;
	}


	//R O T A T I O N 
	// moves mObject according to Vector3 Movement
	movement *= evt.timeSinceLastFrame;
	mObject->translate(movement);


	Degree rotation = Degree(30);
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4))
	{
		mObject->rotate(Vector3(0, 1, 0), Radian(rotation) * evt.timeSinceLastFrame);
	}

	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6))
	{
		mObject->rotate(Vector3(0, 1, 0), Radian(-rotation) * evt.timeSinceLastFrame);
	}

	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8))
	{
		mObject->rotate(Vector3(1, 0, 0), Radian(-rotation) * evt.timeSinceLastFrame);
	}

	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2))
	{
		mObject->rotate(Vector3(1, 0, 0), Radian(rotation) * evt.timeSinceLastFrame);
	}
	return true;

	// moves mObject according to Vector3 Movement
	movement *= evt.timeSinceLastFrame;
	mObject->translate(movement);

	//R E V O L U T I O N
	Degree revolutionDegrees = Degree(45.0f * evt.timeSinceLastFrame);
	float oldX = mObject->getPosition().x;
	float oldZ = mObject->getPosition().z;

	float newX = (oldX * Math::Cos(Radian(revolutionDegrees))) + (oldZ * Math::Sin(Radian(revolutionDegrees)));
	float newZ = (oldX * -Math::Sin(Radian(revolutionDegrees))) + (oldZ * Math::Cos(Radian(revolutionDegrees)));

	mObject->setPosition(newX, 0, newZ);

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
