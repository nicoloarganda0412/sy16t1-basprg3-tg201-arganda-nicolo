#include "player.h"
#include "Bomb.h"



Bomb::Bomb()
{
	mName = "Bomb";
}

void Bomb::use(player* user)
{
	user->takeDamage();
}


Bomb::~Bomb()
{
}
