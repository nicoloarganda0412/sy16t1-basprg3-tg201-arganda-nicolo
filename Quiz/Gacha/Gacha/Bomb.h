#pragma once
#include <string>
#include <iostream>
#include "item.h"

class player;

using namespace std;

class Bomb : public item
{
public:
	Bomb();

	void use(player* user);

	~Bomb();

};

