#pragma once
#include <string>
#include <iostream>
#include "item.h"

class player;

using namespace std;

class Crystal : public item
{
public:
	Crystal();

	void use(player* user);

	~Crystal();
};

