#include "HealthPotion.h"
#include "player.h"



HealthPotion::HealthPotion()
{
	mName = "Health Potion";
}


void HealthPotion::use(player* user)
{
	user->heal();
}

HealthPotion::~HealthPotion()
{
}
