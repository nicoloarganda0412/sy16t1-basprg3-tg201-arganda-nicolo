#pragma once
#include <iostream>
#include <string>
#include "item.h"

class player;

using namespace std;

class HealthPotion : public item
{
public:
	HealthPotion();

	void use(player* user);

	~HealthPotion();



};

