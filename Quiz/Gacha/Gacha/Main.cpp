#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "player.h"
#include "item.h"
#include "Bomb.h"
#include "HealthPotion.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"
#include "Crystal.h"

using namespace std;

void displayStats(player* unit);
item* generateUseRandItem(player* unit);
void classifyItem(item* pulled, int& a, int& b, int&c, int&d, int&e, int&f);
void evaluateEnding(player* unit);
void displayPulls(int a, int b, int c, int d, int e, int f);

int main()
{
	srand(time(0));

	int pulls = 0;

	int potionCount = 0;
	int ssrCount = 0;
	int srCount = 0;
	int rCount = 0;
	int bombCount = 0;
	int crystalCount = 0;

	int freePullsCount = 0;

	player* unit = new player();

	for (;;)
	{
		pulls++;
		
		displayStats(unit);
		cout << "Pulls: " << pulls << endl;

		// Generates Item and uses it 
		item* pulled = generateUseRandItem(unit);

		// Classifies Item
		classifyItem(pulled, potionCount, bombCount, crystalCount, ssrCount, srCount, rCount);

		if (freePullsCount != 0)
		{
			if (freePullsCount < 0)
			{
				freePullsCount = 0;
			}
			else
			{
				freePullsCount -= 1;
			}
		}
		else
		{
			unit->decreaseCrystal();
		}

		// Checks if the item is Crystal
		if (pulled->getName() == "Crystal")
		{
			freePullsCount += 2;
		}

		delete pulled;

		system("pause");
		system("cls");

		// Checks if Player Health and Crystals are Zero
		if (unit->getCrystal() == 0 || unit->getHealthPoints() == 0 || unit->getRarityPoints() == 100)
		{
			break;
		}
		
		system("pause");
		system("cls");
 	}

	evaluateEnding(unit);
	cout << "Pulls: " << pulls << endl;

	displayPulls(rCount, srCount, ssrCount, bombCount, potionCount, crystalCount);
	
	delete unit;

	system("pause");
	return (0);
}

void displayStats(player * unit)
{
	cout << "HP: " << unit->getHealthPoints() << endl;
	cout << "Crystals: " << unit->getCrystal() << endl;
	cout << "Rarity Points: " << unit->getRarityPoints() << endl;
} 

item* generateUseRandItem(player * unit)
{
	item* pulled;

	int dice = rand() % 100 + 1;
	cout << dice << endl;
	// Dice rolls to see what item to pull based on chances given
	if (dice >= 2 && dice <= 10)
	{
		pulled = new SR();
	}
	else if (dice >= 11 && dice <= 51)
	{
		pulled = new R();
	}
	else if (dice <= 67 && dice >= 52)
	{
		pulled = new HealthPotion;
	}
	else if (dice <= 88 && dice >= 68)
	{
		pulled = new Bomb();
	}
	else if (dice <= 100 && dice >= 89)
	{
		pulled = new Crystal();
	}
	else 
	{
		pulled = new SSR();
	}
	

	cout << "You pulled a " << pulled->getName() << endl;
	// Item is used on player
	pulled->use(unit);

	return pulled;
}

void classifyItem(item * pulled, int & a, int & b, int & c, int & d, int & e, int & f)
{
	if (pulled->getName() == "Health Potion")
	{
		a++;
	}
	else if (pulled->getName() == "Bomb")
	{
		b++;
	}
	else if (pulled->getName() == "Crystal")
	{
		c++;
	}
	else if (pulled->getName() == "SSR")
	{
		d++;
	}
	else if (pulled->getName() == "SR")
	{
		e++;
	}
	else if (pulled->getName() == "R")
	{
		f++;
	}
}

void evaluateEnding(player * unit)
{
	if (unit->getHealthPoints() == 0 || unit->getRarityPoints() < 100)
	{
		cout << "You lost! " << endl;
		cout << "============================================" << endl;
		displayStats(unit);
		cout << "============================================" << endl;

	}

	else if (unit->getRarityPoints() >= 100) 
	{
		cout << "You Won! " << endl;
		cout << "============================================" << endl;
		displayStats(unit);
		cout << "============================================" << endl;

	}
}

void displayPulls(int a, int b, int c, int d, int e, int f)
{
	cout << "Items Pulled: " << endl;
	cout << "- - - - - - - - - - - - - - - - " << endl;
	cout << "R: x" << a << endl;
	cout << "SR: x" << b << endl;
	cout << "SSR: x" << c << endl;
	cout << "Bomb: x" << d << endl;
	cout << "Health Potion: x" << e << endl;
	cout << "Crystals: x" << f << endl;
}


