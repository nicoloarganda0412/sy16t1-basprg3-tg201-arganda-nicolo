#pragma once
#include <string>
#include <iostream>

class player;

using namespace std;

class item
{
public:
	item();

	string getName();
	
	virtual void use(player* user);

protected:
	string mName;
};

