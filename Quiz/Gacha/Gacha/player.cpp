#include "player.h"
#include "item.h"

// ITEMS
// SSR - 1
// SR - 2
// R - 3
// Health Potion - 4
// Bomb - 5

player::player()
{
	mHealthPoints = 100;
	mCrystal = 100;
	mRarityPoints = 0;
}

int player::getHealthPoints()
{
	return mHealthPoints;
}

int player::getCrystal()
{
	return mCrystal;
}

int player::getRarityPoints()
{
	return mRarityPoints;
}

void player::heal()
{
	cout << "You were healed for 30 points" << endl;
	mHealthPoints += 30;
}

void player::takeDamage()
{
	cout << "You took damage for 25 points" << endl;
	mHealthPoints -= 25;

	if (mHealthPoints < 0)
	{
		mHealthPoints = 0;
	}
}

void player::increaseRP(int cost)
{
	cout << "You gained " << cost << " Rarity Points" << endl;
	mRarityPoints += cost;
}

void player::increaseCrystal()
{
	cout << "You gained 15 Crystals" << endl;
	mCrystal += 15;
}

void player::decreaseCrystal()
{
	mCrystal -= 5;

	if (mCrystal < 0)
	{
		mCrystal = 0;
	}
}
