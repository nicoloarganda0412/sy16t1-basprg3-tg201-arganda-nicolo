#pragma once
#include "item.h"
#include <iostream>
#include <string>

using namespace std;

class player
{
public:
	player();
	
	int getHealthPoints();
	int getCrystal();
	int getRarityPoints();
	
	
	void heal();
	void takeDamage();
	void increaseRP(int cost);
	void increaseCrystal();
	void decreaseCrystal();

private:
	item *mItem;

	int mHealthPoints;
	int mRarityPoints;
	int mCrystal; 

};

