#include "Planet.h"
#include "BaseApplication.h"
#include "TutorialApplication.h"
#include <math.h>

using namespace Ogre;

Planet::Planet(SceneNode* node)
{
	mNode = node;
	
	
}

// Comments:
// Attaching the Nodes
Planet * Planet::createPlanet(SceneManager *sceneManager, float size, ColourValue colour, Planet* body)
{
	ManualObject* planet = sceneManager->createManualObject();
	
	planet->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	// Creates the Cube (Planet)
	planet->position(-(size / 2), -(size / 2), (size / 2));
	planet->colour(colour);
	planet->position((size / 2), -(size / 2), (size / 2));
	planet->colour(colour);
	planet->position((size / 2), (size / 2), (size / 2));
	planet->colour(colour);
	planet->position(-(size / 2), (size / 2), (size / 2));
	planet->colour(colour);

	planet->position(-(size / 2), -(size / 2), -(size / 2));
	planet->colour(colour);
	planet->position((size / 2), -(size / 2), -(size / 2));
	planet->colour(colour);
	planet->position((size / 2), (size / 2), -(size / 2));
	planet->colour(colour);
	planet->position(-(size / 2), (size / 2), -(size / 2));
	planet->colour(colour);

	// Index Buffers
	planet->index(0);
	planet->index(1);
	planet->index(2);

	planet->index(3);
	planet->index(0);
	planet->index(2);

	planet->index(4);
	planet->index(0);
	planet->index(3);

	planet->index(7);
	planet->index(4);
	planet->index(3);

	planet->index(6);
	planet->index(5);
	planet->index(4);

	planet->index(6);
	planet->index(4);
	planet->index(7);

	planet->index(6);
	planet->index(1);
	planet->index(5);

	planet->index(1);
	planet->index(6);
	planet->index(2);

	planet->index(6);
	planet->index(7);
	planet->index(3);

	planet->index(2);
	planet->index(6);
	planet->index(3);

	planet->index(4);
	planet->index(1);
	planet->index(0);

	planet->index(4);
	planet->index(5);
	planet->index(1);

	planet->end();

	// To Attach
	body->mNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	body->mNode->attachObject(planet);
	return body;

}


void Planet::revolvePlanet(SceneNode* sceneNode, float revolutionSpeed)
{
	Degree rev = Degree(revolutionSpeed);
	float xPrevious = sceneNode->getPosition().x;
	float zPrevious = sceneNode->getPosition().z;

	// creates the next set of coordinates
	float xNext = (xPrevious * Math::Cos((Radian(rev))) + zPrevious * Math::Sin(Radian(rev)));
	float zNext = (xPrevious * -Math::Sin((Radian(rev))) + zPrevious * Math::Cos(Radian(rev)));

	sceneNode->setPosition(xNext, 0, zNext);

}

void Planet::revolveMoon(SceneNode * Earth, SceneNode * sceneNode, float revolutionSpeed)
{
	Degree rev = Degree(revolutionSpeed);
	float xPrevious = Earth->getPosition().x;
	float zPrevious = Earth->getPosition().z;

	// creates the next set of coordinates
	float xNext = (xPrevious * Math::Cos((Radian(rev))) + zPrevious * Math::Sin(Radian(rev)));
	float zNext = (xPrevious * -Math::Sin((Radian(rev))) + zPrevious * Math::Cos(Radian(rev)));

	sceneNode->setPosition(xNext, 0, zNext);

}

void Planet::rotatePlanet(SceneNode* sceneNode, float degrees)
{

	Degree rot = Degree(degrees);
	sceneNode->rotate(Vector3(0, 1, 0), Radian(rot));

}

void Planet::translate(int x, int y, int z)
{
	mNode->translate(x, y, z);
}

SceneNode * Planet::getSceneNode()
{
	return mNode;
}

