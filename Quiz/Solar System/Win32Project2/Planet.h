#pragma once
#include "BaseApplication.h"
#include "TutorialApplication.h"

using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode* node);
	static Planet * createPlanet(SceneManager *sceneManager, float size, ColourValue colour, Planet* body);
	static void revolvePlanet(SceneNode* sceneNode, float revolutionSpeed);
	static void revolveMoon(SceneNode* Earth, SceneNode* sceneNode, float revolutionSpeed);
	static void rotatePlanet(SceneNode* sceneNode, float degrees);
	void translate(int x, int y, int z);
	SceneNode* getSceneNode();

protected: 
	SceneNode* mNode;
};

