/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "Planet.h"

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	//pointer
	mCamera->setPolygonMode(PolygonMode::PM_SOLID);

	Planet* Sun = new Planet(mSun);
	Sun->createPlanet(mSceneMgr, 20.0f, ColourValue::Blue, Sun);
	mSun = Sun->getSceneNode();
	Sun->translate(0, 0, 0);

	Planet* Mercury = new Planet(mMercury);
	Mercury->createPlanet(mSceneMgr, 3.0f, ColourValue::Red, Mercury);
	mMercury = Mercury->getSceneNode();
	Mercury->translate(20, 0, 20);

	Planet* Venus = new Planet(mVenus);
	Venus->createPlanet(mSceneMgr, 5.0f, ColourValue::Green, Venus);
	mVenus = Venus->getSceneNode();
	Venus->translate(40, 0, 40);

	Planet* Earth = new Planet(mEarth);
	Earth->createPlanet(mSceneMgr, 10.0f, ColourValue::Blue, Earth);
	mEarth = Earth->getSceneNode();
	Earth->translate(60, 0, 60);

	Planet* Mars = new Planet(mMars);
	Mars->createPlanet(mSceneMgr, 10.0f, ColourValue::Red, Mars);
	mMars = Mars->getSceneNode();
	Mars->translate(100, 0, 100);

	Planet* Moon = new Planet(mMoon);
	Moon->createPlanet(mSceneMgr, 1.0f, ColourValue::White, Moon);
	mMoon = Moon->getSceneNode();
	Moon->translate(0, 0, 0);
	
}
bool TutorialApplication::keyReleased(const OIS::KeyEvent & args)
{
	return true;
}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	// R O T A T I O N
	Planet::rotatePlanet(mSun, 45.0f * evt.timeSinceLastFrame);
	Planet::rotatePlanet(mEarth, 45.0f * evt.timeSinceLastFrame);
	Planet::rotatePlanet(mMercury, 45.0f * evt.timeSinceLastFrame);
	Planet::rotatePlanet(mVenus, 45.0f * evt.timeSinceLastFrame);
	Planet::rotatePlanet(mMars, 45.0f * evt.timeSinceLastFrame);
	Planet::rotatePlanet(mMoon, 45.0f * evt.timeSinceLastFrame);

	// "normalize"
	// R E V O L V E
	Planet::revolvePlanet(mMercury, 48.9f *evt.timeSinceLastFrame);
	Planet::revolvePlanet(mVenus, 35.0f *evt.timeSinceLastFrame);
	Planet::revolvePlanet(mEarth, 29.8f *evt.timeSinceLastFrame);
	Planet::revolvePlanet(mMars, 24.2f *evt.timeSinceLastFrame);
	
	Planet::revolveMoon(mEarth, mMoon, 50.0f);

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
